import csv
import sys
import networkx as nx
import matplotlib.pyplot as plt
from networkx.algorithms import community
G=nx.Graph()
f=open('casts.csv', 'r')
reader = csv.reader(f,delimiter=';')
lastMovie='';
lastMovieActors=[]
nodes=[]
sprin_pos= nx.spring_layout(G)
for row in reader:
    G.add_node(row[2])
    nodes.append(row[2])
    if lastMovie!=row[1].lower():
        lastMovie=row[1].lower()
        lastMovieActors=[]
    for i in lastMovieActors:
        G.add_edge(row[2],i)
    lastMovieActors.append(row[2])

f.close()

communities = {node:cid+1 for cid,community in enumerate(nx.algorithms.community.k_clique_communities(G,3)) for node in community}
centralities=[]

deg_centrality=nx.degree_centrality(G)
centralities.append(deg_centrality)
eigenvector_centrality=nx.eigenvector_centrality(G)
centralities.append(eigenvector_centrality)
betweenness_centrality=nx.betweenness_centrality(G)
centralities.append(betweenness_centrality)
load_centrality=nx.load_centrality(G)
centralities.append(load_centrality)

for centrality in centralities:
    bestKeyPlayer=''
    bestValue=0;
    secondBestPl=''
    secondBestValue=0;
    for node in nodes:
        G.node[node]['centrality']=centrality[node]
        if centrality[node]>bestValue:
            if node=="s a":
                continue
            secondBestPl=bestKeyPlayer
            secondBestValue=bestValue
            bestKeyPlayer=node
            bestValue=centrality[node]
    print("Best Key Player:" + bestKeyPlayer + " " + str(bestValue))
    print("Best Key Player:" + secondBestPl + " " + str(secondBestValue))
average=0;
counter=0;
min=5;
minNode=''
maxNode='';
max=0;
print(nx.info(G))
print(nx.density(G))
print("NX components" + str(nx.number_connected_components(G)))
f.close()
for node in nodes:
    try:
        shortestLength=nx.shortest_path_length(G,"Humphrey Bogart", node)
        if(shortestLength==0):
            continue
        average+=shortestLength;
        if(shortestLength>max):
            max=shortestLength
            maxNode=node
        if(shortestLength<min):
            min=shortestLength
            minNode=node
    except nx.NetworkXNoPath:
        counter=counter+1
average=average/(len(nodes)-counter)
print("Min:" + str(min) + ", Max:" + str(max) + ", Average:"  + str(average))
print(minNode)
print(maxNode)
nx.write_gexf(G, "export.gexf")