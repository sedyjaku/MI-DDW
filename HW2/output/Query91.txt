TF-IDF:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
BINARY:
    Euclidean distance:
         F-Measure:0.05454545454545455         Precision:0.03         Recall:0.3
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
PURE TF:
    Euclidean distance:
         F-Measure:0.03636363636363636         Precision:0.02         Recall:0.2
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
