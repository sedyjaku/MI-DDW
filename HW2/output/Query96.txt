TF-IDF:
    Euclidean distance:
         F-Measure:0.05263157894736842         Precision:0.03         Recall:0.21428571428571427
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
BINARY:
    Euclidean distance:
         F-Measure:0.03508771929824562         Precision:0.02         Recall:0.14285714285714285
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
PURE TF:
    Euclidean distance:
         F-Measure:0.01754385964912281         Precision:0.01         Recall:0.07142857142857142
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
