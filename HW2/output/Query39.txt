TF-IDF:
    Euclidean distance:
         F-Measure:0.07017543859649124         Precision:0.04         Recall:0.2857142857142857
    Cosine similarity:
         fMeasure:0.03508771929824562         Precision:0.02         Recall:0.14285714285714285
BINARY:
    Euclidean distance:
         F-Measure:0.01754385964912281         Precision:0.01         Recall:0.07142857142857142
    Cosine similarity:
         fMeasure:0.03508771929824562         Precision:0.02         Recall:0.14285714285714285
PURE TF:
    Euclidean distance:
         F-Measure:0.03508771929824562         Precision:0.02         Recall:0.14285714285714285
    Cosine similarity:
         fMeasure:0.03508771929824562         Precision:0.02         Recall:0.14285714285714285
