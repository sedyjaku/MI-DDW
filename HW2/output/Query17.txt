TF-IDF:
    Euclidean distance:
         F-Measure:0.038834951456310676         Precision:0.02         Recall:0.6666666666666666
    Cosine similarity:
         fMeasure:0.019417475728155338         Precision:0.01         Recall:0.3333333333333333
BINARY:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0.019417475728155338         Precision:0.01         Recall:0.3333333333333333
PURE TF:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0.019417475728155338         Precision:0.01         Recall:0.3333333333333333
