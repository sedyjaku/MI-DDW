TF-IDF:
    Euclidean distance:
         F-Measure:0.056603773584905655         Precision:0.03         Recall:0.5
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
BINARY:
    Euclidean distance:
         F-Measure:0.018867924528301886         Precision:0.01         Recall:0.16666666666666666
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
PURE TF:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0         Precision:0.0         Recall:0.0
