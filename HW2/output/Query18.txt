TF-IDF:
    Euclidean distance:
         F-Measure:0.019230769230769232         Precision:0.01         Recall:0.25
    Cosine similarity:
         fMeasure:0.019230769230769232         Precision:0.01         Recall:0.25
BINARY:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0.019230769230769232         Precision:0.01         Recall:0.25
PURE TF:
    Euclidean distance:
         F-Measure:0         Precision:0.0         Recall:0.0
    Cosine similarity:
         fMeasure:0.019230769230769232         Precision:0.01         Recall:0.25
