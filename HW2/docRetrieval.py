# import
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
from sklearn.metrics.pairwise import euclidean_distances
import numpy as np
 
    
def countFMeasure(precision,recall):
    if ((precision==0) and (recall==0)):
        return 0
    fMeasure=2*(precision*recall)/(precision+recall)
    return fMeasure
    
def countPrecision(truePositive,falsePositive):
    precision=truePositive/(truePositive+falsePositive)
    return precision
    

def countRecall(truePositive,falseNegative):
    recall=truePositive/(truePositive+falseNegative)
    return recall

def countFalsePositive(relevancy,relevancySolution):
    counter=0
    for i in relevancy:
        if i not in relevancySolution:
            counter+=1
    return counter

def countTruePositive(relevancy,relevancySolution):
    counter=0
    for i in relevancy:
        if i in relevancySolution:
            counter+=1
    return counter

def countFalseNegative(relevancy,relevancySolution):
    counter=0
    for i in relevancySolution:
        if i not in relevancy:
            counter+=1
    return counter

def countTrueNegative(relevancy,relevancySolution):
    counter=0
    for i in relevancySolution:
        if i not in relevancy:
            counter+=1
    return counter    
    
def computeSimilarity(tfidf_matrix,corpus,relevant,file):
    cosSim = np.array(cosine_similarity(tfidf_matrix[len(corpus)-1], tfidf_matrix[0:(len(corpus)-1)])[0])
    eucSim = np.array(euclidean_distances(tfidf_matrix[len(corpus)-1], tfidf_matrix[0:(len(corpus)-1)])[0])
    topRelevantCos = cosSim.argsort()[-100:][::-1]+1
    topRelevantEuc = eucSim.argsort()[-100:][::-1]+1
    truePositiveCntrCos=countTruePositive(topRelevantCos,relevant)
    falsePositiveCntrCos=countFalsePositive(topRelevantCos,relevant)
    trueNegativeCntrCos=countTrueNegative(topRelevantCos,relevant)
    falseNegativeCntrCos=countFalseNegative(topRelevantCos,relevant)
    truePositiveCntrEuc=countTruePositive(topRelevantEuc,relevant)
    falsePositiveCntrEuc=countFalsePositive(topRelevantEuc,relevant)
    trueNegativeCntrEuc=countTrueNegative(topRelevantEuc,relevant)
    falseNegativeCntrEuc=countFalseNegative(topRelevantEuc,relevant)
    precisionEuc=countPrecision(truePositiveCntrEuc,falsePositiveCntrEuc)
    recalEuc=countRecall(truePositiveCntrEuc,falseNegativeCntrEuc)
    precisionCos=countPrecision(truePositiveCntrCos,falsePositiveCntrCos)
    recalCos=countRecall(truePositiveCntrCos,falseNegativeCntrCos)
    fMeasureCos=countFMeasure(precisionCos,recalCos)
    fMeasureEuc=countFMeasure(precisionEuc,recalEuc)
    
    
    file.write("    Euclidean distance:\n")
    file.write("         F-Measure:" + str(fMeasureEuc))
    file.write("         Precision:" + str(precisionEuc))
    file.write("         Recall:" + str(recalEuc)+"\n")
    
    
    file.write("    Cosine similarity:\n")
    file.write("         fMeasure:" + str(fMeasureCos))
    file.write("         Precision:" + str(precisionCos))
    file.write("         Recall:" + str(recalCos)+"\n")
    

   
    return





# prepare corpus
corpus = []
vocabulary
for d in range(1400):
    f = open("./d/"+str(d+1)+".txt")
    corpus.append(f.read())
# add query to corpus
for q in range(225):
    f = open("./q/"+str(q+1)+".txt")
    vocabulary = ((f.read()).split())
    corpus.append(f.read())
    relevant =[]
    f = open("./r/"+str(q+1)+".txt")
    relevant = [int(i) for i in f.read().split()]
    # init vectorizer
    count_vectorizer = CountVectorizer()
    binary_vectorizer = CountVectorizer(binary=True)
    tfidf_vectorizer = TfidfVectorizer()
    file = open("./output/Query"+str(q+1)+".txt",'w+') 
    
 
    

    

    # prepare TF-IDF matrix
    tfidf_matrix = tfidf_vectorizer.fit_transform(corpus)
     # compute similarity between query and all docs (tf-idf) and get top 10 relevant
    file.write("TF-IDF:\n") 
    computeSimilarity(tfidf_matrix,corpus,relevant,file)
    # prepare BINARY matrix
    tfidf_matrix = binary_vectorizer.fit_transform(corpus)
     # compute similarity between query and all docs (BINARY) and get top 10 relevant
    file.write("BINARY:\n") 
    computeSimilarity(tfidf_matrix,corpus,relevant,file)
    # prepare pure TF matrix
    tfidf_matrix = count_vectorizer.fit_transform(corpus)
     # compute similarity between query and all docs (pure TF) and get top 10 relevant
    file.write("PURE TF:\n") 
    computeSimilarity(tfidf_matrix,corpus,relevant,file)
    
    file.close() 
