import wikipedia
import nltk

def getFirstWikiSentence(name):
    sentence = "Thing"
    try:
        sentence = wikipedia.summary(name, sentences=1)
    # except wikipedia.DisambiguationError as e:
    #     summary = wikipedia.summary(e.options[0], sentences=1)
    # except wikipedia.PageError:
    #     return None
    except:
        pass
    return sentence


def findDescription(name):
    grammar = r"""
      NP: {<DT|PP\$>?<JJ>*<NN>}   # chunk determiner/possessive, adjectives and noun
          {<NNP>+}                # chunk sequences of proper nouns
    """
    results = wikipedia.search(name)
    sentence = getFirstWikiSentence(name)
    tokens = nltk.word_tokenize(sentence)
    tagged =nltk.pos_tag(tokens)
    parser =nltk.RegexpParser(grammar)
    tree = parser.parse(tagged)
    printing=False
    output=''

    for i in range(len(tree)):
        if isinstance(tree[i][0], str):
            if(printing):
                output=output+tree[i][0]+' '
            else:
                if(tree[i][0]=='is'):
                    printing=True

        else:
            if(printing):
                for j in range(len(tree[i])):
                    output=output+tree[i][j][0]+' '
                break
    return output

def getEntityName(entity):
    name=''
    for i in range(len(entity)):
        name=name+entity[i][0]+' '
    return name


file = open("testfile.txt", "r") 
text=file.read()
tokens =nltk.word_tokenize(text)
tagged =nltk.pos_tag(tokens)
entity =[]
entities = []
for tagged_entry in tagged:
    if((tagged_entry[1].startswith("JJ")) or (tagged_entry[1].startswith("NNP"))):
        entity.append(tagged_entry)
    else:
        if (entity):
            if(entity[-1][1].startswith("JJ")):
                entity.pop()
            else:
                entities.append(entity)
        entity =[]
for entity in entities:
    entityName=getEntityName(entity)
    print(entityName+':'+findDescription(entityName))